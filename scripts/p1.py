import json


class Conversion:
    @staticmethod
    def to_json(enrolling):
        j = json.dumps(enrolling, indent=4)
        return j
